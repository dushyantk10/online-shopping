class UserMailer < ActionMailer::Base
	default :from => "last71637@gmail.com"

	def registration_confirmation(user)
		@user = user
		mail(:to => user.email, :subject => "Please confirm your registration")
	end
end