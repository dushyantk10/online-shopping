class UsersController < ApplicationController
  def home
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      SendEmailJobJob.perform_later(@user)
      log_in @user
      flash[:sucess] = "Please Confirm your email and continue"
      redirect_to root_url
    else
      render 'new'
    end
  end

#   def confirm_email
#     user = User.find_by_confirm_token(params[:id])
#     if user
#       user.email_activate
#       flash[:success] = "Welcome to the Sample App! Your email has been confirmed.
#       Please sign in to continue."
#       redirect_to signin_url
#     else
#       flash[:error] = "Sorry. User does not exist"
#       redirect_to root_url
#     end
# end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end
end