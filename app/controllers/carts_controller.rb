class CartsController < ApplicationController

  def create
    cart = current_user.cart.create(cart_params)
    render json: cart
  end

  def cart_for_user
    carts = current_user.carts
    render json: carts
  end
  
  private
  
  def cart_params
    params.permit(:product_id)
  end
end
