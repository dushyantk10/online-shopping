class ProductsController < ApplicationController
  
  def index
    products = Product.all
    render json: products
  end

  def product_with_category
    category = Category.find(id: params[:category_id])
    products = category.products
    render json: products
  end

end