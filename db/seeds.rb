Faker::Config.locale = :en
15.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password: password,
               password_confirmation: password)
end

users=User.all

users.each do |user|
  5.times do
    body = Faker::Lorem.sentence(10)
    user.questions.create!(body: body)
  end
end

questions=Question.all

questions.each do |question|
  10.times do
    body = Faker::Lorem.sentence(20)
    uid=Random.rand(1..15)
    question.answers.create!(user_id: uid,body: body)
  end
end

questions.each do |question|
  5.times do
    body = Faker::Lorem.sentence(10)
    uid=Random.rand(1..15)
    question.comments.create!(user_id: uid,body: body)
  end
end

answers=Answer.all

answers.each do |answer|
  5.times do
    body = Faker::Lorem.sentence(10)
    uid=Random.rand(1..15)
    answer.comments.create!(user_id: uid,body: body)
  end
end