Rails.application.routes.draw do
  get 'sessions/new'
  root 'users#home'
  get '/signup', to:'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users do
     member do
        get :confirm_email
     end
  end
  resources :categories, only: [:index]
  resources :products, only: [:index] do
   get :product_with_category
  end
  resources :categories, only: [:create] do
   get :cart_for_user
  end
end
